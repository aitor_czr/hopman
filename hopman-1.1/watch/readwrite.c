/* See LICENSE file for copyright and license details. */ 
#include <unistd.h>
#include <errno.h>
#include "hopman.h"

/*======== read the exact requested size into a buffer - or until EOF =======*/
/* same as read() but recovers from EINTR */
int pm_read(int fd, void *buf, size_t count)
{
  size_t n, p;
  for(n=p=0; n<count; n+=p)
    {
      p=read(fd, ((char*)buf)+n, count-n);
      if(p<0 && errno != EINTR) return -1;
      if(p==0) break; /* eof */
    }
  return n;
}

/*======== write the exact requested size from a buffer - or until EOF ======*/
/* same as write() but recovers from EINTR */
int pm_write(int fd, void *buf, size_t count)
{
  size_t n, p;
  for(n=p=0; n<count; n+=p)
    {
      p=write(fd, ((char*)buf)+n, count-n);
      if(p<0 && errno != EINTR) return -1;
      if(p==0) break; /* eof */
    }
  return n;
}
