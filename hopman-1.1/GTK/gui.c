#include <gtk/gtk.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "hopman.h"
#include "gui.h"

static int NP = 0; /* count number of partitions */

void ui_create(partinfo_t *p)
{
  GtkWidget *pMenuItem;
  GtkWidget *pMenu;
  GtkWidget *pHbox;
  GtkWidget *pLname=NULL, *pLlabel=NULL, *pLfstype=NULL, *pLmnt=NULL;
  GtkWidget *pMount=NULL, *pUmount=NULL, *pOpen=NULL, *pTerm=NULL;
  GtkWidget *pEject=NULL;
  guint myshowflags = showflags;
  
  NP ++;

  /* create the action menu and associated submenu item to put it in menubar */
  pMenu     = gtk_menu_new();
  pMenuItem = gtk_menu_item_new();
  
  gtk_menu_shell_prepend( GTK_MENU_SHELL(pList), pMenuItem );
  gtk_menu_item_set_submenu( GTK_MENU_ITEM(pMenuItem), pMenu );

  /* create an hbox to fill the label of the menuitem */
#if GTK_MAJOR_VERSION == 2
  pHbox = gtk_hbox_new(FALSE, 8); 
#elif GTK_MAJOR_VERSION == 3
  pHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 8);   
#endif
  gtk_container_add(GTK_CONTAINER(pMenuItem), pHbox);

  /* fill the hbox with partition's name, label, fstype, mountpoint */
  if(showflags & SHOWNAME)
    {
      pLname = gtk_label_new(p->name);
      gtk_box_pack_start(GTK_BOX(pHbox), pLname, FALSE, FALSE, 0);
    }
  if((showflags & SHOWLABEL) || !(showflags & SHOWNAME) )
    {
      /* if name is not shown, label is shown - replaced by name if absent */
      if(p->label[0] || (showflags & SHOWNAME))
	pLlabel = gtk_label_new(p->label);
      else  pLlabel = gtk_label_new(p->name);
      gtk_box_pack_start(GTK_BOX(pHbox), pLlabel, FALSE, FALSE, 0);
    }
  if(myshowflags & SHOWFSTYPE)
    {
      pLfstype = gtk_label_new(p->fstype);
      gtk_box_pack_start(GTK_BOX(pHbox), pLfstype, FALSE, FALSE, 0);
    }
  if(myshowflags & SHOWMNT)
    {
      char *Item = (char*) malloc(sizeof(char) * 256);
      if(!Item) 
        {
          fprintf(stderr, _("%s: Memory allocation failure.\n"), progname);
          exit(EXIT_FAILURE);
        }
      pLmnt = gtk_label_new(NULL);      
      strcpy(Item, p->mnt);
#if GTK_MAJOR_VERSION == 2
      GdkColor color;
      gdk_color_parse ("blue", &color);
      gtk_widget_modify_fg (pLmnt, GTK_STATE_NORMAL, &color);
#elif GTK_MAJOR_VERSION == 3
      GdkRGBA rgba;
      gdk_rgba_parse (&rgba, "blue");
      gtk_widget_override_color (pLmnt, 0, &rgba);
      //gtk_widget_modify_fg (pLmnt, GTK_STATE_NORMAL, &rgba); 
#endif
      gtk_label_set_markup_with_mnemonic(GTK_LABEL(pLmnt), Item);
      gtk_container_add (GTK_CONTAINER (pHbox), pLmnt);
      free(Item);
    }

  /* create the menuitems for the submenu and connect them to the callbacks */
  pOpen  = gtk_menu_item_new_with_label(_("Open in file-manager"));
  gtk_menu_shell_append(GTK_MENU_SHELL(pMenu), pOpen);
  g_signal_connect(G_OBJECT(pOpen), "activate", G_CALLBACK(on_click_open),
		   (gpointer)p );
  pTerm  = gtk_menu_item_new_with_label(_("Open in terminal"));
  gtk_menu_shell_append(GTK_MENU_SHELL(pMenu), pTerm);
  g_signal_connect(G_OBJECT(pTerm), "activate", G_CALLBACK(on_click_term),
		   (gpointer)p );
  pMount = gtk_menu_item_new_with_label(_("Mount"));
  gtk_menu_shell_append(GTK_MENU_SHELL(pMenu), pMount);
  g_signal_connect(G_OBJECT(pMount), "activate", G_CALLBACK(on_click_mount),
		   (gpointer)p );
  pUmount= gtk_menu_item_new_with_label(_("Umount"));
  gtk_menu_shell_append(GTK_MENU_SHELL(pMenu), pUmount);
  g_signal_connect(G_OBJECT(pUmount), "activate", G_CALLBACK(on_click_umount),
		   (gpointer)p );
  pEject= gtk_menu_item_new_with_label("_(Eject)");
  gtk_menu_shell_append(GTK_MENU_SHELL(pMenu), pEject);
  g_signal_connect(G_OBJECT(pEject), "activate", G_CALLBACK(on_click_eject),
		   (gpointer)p );

  if( p->nmounts )  gtk_widget_set_sensitive(pMount, FALSE);
  else gtk_widget_set_sensitive(pUmount, FALSE);
  /* save widget pointers in object, so that we can later destroy them */
  p->uiptr[0]  = pMenuItem;
  p->uiptr[1]  = pMenu;
  p->uiptr[2]  = pHbox;
  p->uiptr[3]  = pLname;
  p->uiptr[4]  = pLlabel;
  p->uiptr[5]  = pLfstype;
  p->uiptr[6]  = pLmnt;
  p->uiptr[7]  = pMount;
  p->uiptr[8]  = pUmount;
  p->uiptr[9]  = pOpen;
  p->uiptr[10] = pTerm;
  p->uiptr[11] = NULL; /* no use for this pointer */
  gtk_widget_show_all(pList);
  ui_set_visible(1);
}

void ui_delete(partinfo_t *p)
{
  int i;
  /* remove the item pMenuItem from the menubar */ 
  gtk_container_remove( GTK_CONTAINER(pList), (GtkWidget *)p->uiptr[0] );
  /* separator */
  gtk_container_remove( GTK_CONTAINER(pList), GTK_WIDGET(p->uiptr[11]) );
  
  /* destroy all widgets */
  for(i=11; i<=0; i--)
    {
      if( p->uiptr[i] )    
#if GTK_MAJOR_VERSION == 2
	    gtk_object_destroy( (GtkObject *)(p->uiptr[i]) );  
#elif GTK_MAJOR_VERSION == 3
	    gtk_widget_destroy( (GtkWidget *)(p->uiptr[i]) );    
#endif
    }

  NP --;

  gtk_widget_show_all(pList);
  if(NP == 0 && (showflags & AUTOHIDE)) ui_set_visible(0);
}

void ui_update(partinfo_t *p)
{
  GtkWidget *pLname, *pLlabel, *pLfstype, *pLmnt, *pMount, *pUmount;
  guint myshowflags;
  
  /* change label, fstype and mountpoint in the menuitem */
  pLname   = p->uiptr[3];
  pLlabel  = p->uiptr[4];
  pLfstype = p->uiptr[5];
  pLmnt    = p->uiptr[6];
  pMount   = p->uiptr[7];
  pUmount  = p->uiptr[8];

  if(pLlabel)
    {
      if(p->label[0] || pLname)
	gtk_label_set_text( GTK_LABEL(pLlabel), p->label );
      else
	gtk_label_set_text( GTK_LABEL(pLlabel), p->name );
    }

  if(pLfstype)  gtk_label_set_text( GTK_LABEL(pLfstype), p->fstype );
  if(p->nmounts>1 && pLmnt)
    {
      /* prepend a * to the mountpoint path if more than one mountpoint */
      char buf[PM_NAME_LEN+2];
      buf[0] = '*';
      strcpy(buf+1, p->mnt);
      gtk_label_set_text( GTK_LABEL(pLmnt), buf );
    }
  else if( pLmnt ) gtk_label_set_text( GTK_LABEL(pLmnt), p->mnt );

  if( p->nmounts )
    {
      gtk_widget_set_sensitive(pMount, FALSE);
      gtk_widget_set_sensitive(pUmount, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive(pMount, TRUE);
      gtk_widget_set_sensitive(pUmount, FALSE);
    }
  gtk_widget_show_all(pList);
}

void ui_msg(const char *msg)
{
  GtkWidget *dialog;
  /* open a popup window and display the message */
  dialog = gtk_message_dialog_new( GTK_WINDOW(pWindow),
				   GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_ERROR,
				   GTK_BUTTONS_CLOSE,
				   msg );
  gtk_dialog_run( GTK_DIALOG(dialog) );
  gtk_widget_destroy( dialog );
}

/*===================== The user interaction callbacks ======================*/

void on_click_open ( GtkWidget *pWidget, gpointer pData )
{
  partinfo_t *p = (partinfo_t *)pData;

  execute(Open_in_File_Manager, p);
}

void on_click_term ( GtkWidget *pWidget, gpointer pData )
{
  partinfo_t *p = (partinfo_t *)pData;

  execute(Open_in_Terminal, p);
}

void on_click_mount ( GtkWidget *pWidget, gpointer pData )
{
  partinfo_t *p = (partinfo_t *)pData;

  execute(Mount, p);
}

void on_click_umount ( GtkWidget *pWidget, gpointer pData )
{
  partinfo_t *p = (partinfo_t *)pData;

  execute(Unmount, p);
}

void on_click_eject ( GtkWidget *pWidget, gpointer pData )
{
  partinfo_t *p = (partinfo_t *)pData;

  execute(Eject, p);
}
