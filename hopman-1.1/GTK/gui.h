#if !defined(UI_GTK2_H)
#define UI_GTK2_H

void set_icon(GtkWindow *);

void on_click_open  ( GtkWidget *pWidget, gpointer pData );
void on_click_term  ( GtkWidget *pWidget, gpointer pData );
void on_click_mount ( GtkWidget *pWidget, gpointer pData );
void on_click_umount( GtkWidget *pWidget, gpointer pData );
void on_click_eject ( GtkWidget *pWidget, gpointer pData );

/* The following widget is initialized by main(); pList is the menubar
   the items of which are partitions. The ui hooks declared in ../hopman.h
   are in charge of updating the items of this menubar. Each item is a
   submenu of user actions. */

# ifdef MAIN_PROGRAM
GtkWidget *pWindow;
GtkWidget *pList; 
guint showflags;
# else
extern GtkWidget *pWindow;
extern GtkWidget *pList; 
extern guint showflags;
# endif
#endif
