/*-----------------------------------------------------------------------------
  Copyright (C) 2019 Didier Kryn <kryn@in2p3.fr>
  
  Permission to  use, copy, modify, distribute, and sell this software and its
  documentation for any purpose  is hereby granted without fee,  provided that
  the above copyright notice appear in all copies and that both that copyright
  notice and this permission notice appear in supporting documentation.
  
  The above copyright notice  and this permission notice  shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED  "AS IS",  WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING  BUT NOT LIMITED  TO  THE WARRANTIES OF  MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE  AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES  OR OTHER  LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
  IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  
  Except as contained  in this notice, the name of copyright holders shall not
  be used  in  advertising  or  otherwise  to promote  the sale,  use or other
  dealings  in this  Software  without prior  written  authorization  from the
  copyright holders.
  ---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  This header file contains declarations common to the partition-watching
  and command-submission machineries and the UI.
  ---------------------------------------------------------------------------*/

#if !defined(HOPMAN_H)
#define HOPMAN_H
#include <sys/types.h>
#include <linux/limits.h>
#define PM_NAME_LEN 15
#define PM_LABEL_LEN 25
#define PM_FSTYPE_LEN 15
#define PM_MNT_LEN NAME_MAX

/* interval of periodic read of /proc/self/mountinfo: */
#define PM_ITIMERVAL 5

/*--------- Declaration of the object representing a partition ---------*/
typedef
struct
{
  ino_t inode;
  unsigned nmounts;
  char name[PM_NAME_LEN+1];
  char label[PM_LABEL_LEN+1];
  char fstype[PM_FSTYPE_LEN+1];
  char mnt[PM_MNT_LEN+1];
  void *uiptr[16];       /* A provision for the private use of the UI */
} partinfo_t;

typedef
enum {Open_in_File_Manager, Open_in_Terminal, Mount, Unmount, Eject} command_t;

/*====================== Functions the UI shall provide =====================*/
void ui_create(partinfo_t *p); /* invoked when a partition was inserted  */
void ui_delete(partinfo_t *p); /* invoked when a partition was removed   */
void ui_update(partinfo_t *p); /* invoked when partition status changed  */
void ui_set_visible(int);  /* 0->hide, 1->show, -1->toggle window visibility */
void ui_msg(const char *msg);  /* display a message */

/*======================= Functions the UI shall invoke =====================*/
/* By main() at start up */
int userinfo_init(void);     /* At the beginning, obtain username ahd home   */
/*** config_read() -- see below configuration file interface. ***/
void newpid(void);           /* kill previous instance and rewrite pid file  */
int inotinit(void);   /* return input file descriptor to watch in main loop  */
int signalinit(void); /* return input file descriptor to watch in main loop  */
int mountinit(void);  /* open /proc/self/mountinfo and return filedescriptor */

/* By main() just before entering iddle loop */
void init_list(void); /* Detect hotplug partitions present at start up */

/* By UI callbacks connected to watched input file descriptors        */
void inotify_read( int inotfd ); /* read & process inotify events     */
void signal_read( int sfd );     /* read & process signals            */
void mountpoints( void );        /* read mountinfo and update display */
partinfo_t *get_partition(ino_t node);

/* By UI's user action call-backs associated to menus */
void execute(command_t cmd, partinfo_t *p);


/*--------------- Interface with the configuration file --------------------*/
/* parameters:,  IDs, names and default values: */
typedef enum paramid_t
  {
    MountHelper, UmountHelper, EjectHelper, FileManager, TerminalEmulator,
    MountCheckPeriod, ShowName, ShowLabel, ShowFstype, ShowMountPoint, Sticky,
    PidFile, AutoHide, ToggleHide, Decorate, KeepPosition, InitialPosition,
    Debug, N_CONFIG_PARAMS
  } paramid_t;

#define PARAM_NAMES "mounthelper", "umounthelper", "ejecthelper", \
    "filemanager", "terminalemulator", "mountcheckperiod", \
    "showname", "showlabel", "showfstype", "showmountpoint", "sticky", \
    "pidfile", "autohide", "togglehide", "decorate", "keepposition", \
    "initialposition", "debug"

#define PARAM_DEFAULTS "exec pmount %n %l", "exec pumount %n", \
    "" , "exec thunar", "exec xfce4-terminal", "5", \
    "false", "false", "false", "true", "true", "%h/hopman-pid", \
    "true", "true", "true", "true", "mouse", "false"

/* Comment: there is currently no known eject-helper */
int config_read(const char *path); /* return -1 in case of error, else 0 */
const char *config_string(paramid_t param);
int config_int(paramid_t param); /* -1 in case of error */
int config_bool(paramid_t param); /* 1 for "true", 0 for "false" else -1 */
/* config_bool() is case-insensitive */
unsigned config_show(void); /* provide configuration for the UI */
#define SHOWNAME   1
#define SHOWLABEL  2
#define SHOWFSTYPE 4
#define SHOWMNT    8
#define STICKY    16
#define AUTOHIDE  32

/* In the 2 functions below, buf must be large enough to store the resulting 
   string.                                                                   */
const char *config_pidfile(char *buf, size_t len);
const char *config_helper(command_t cmd, partinfo_t *p, char *buf, size_t len);
const char *username(void);
const char *userhome(void);

void do_all_config(void);/* invoke userinfo_init(), config_read() & newpid() */
/*-------------------------- Global constant --------------------------------*/
#if !defined(MAIN_PROGRAM)
extern
#endif
const char *progname; /* main() is expected to set this string */

/*--------------------- Internatioanlization --------------------------------*/
#include <locale.h>
#include <libintl.h>
#define _(StRiNg)  dgettext("hopman",StRiNg)
#endif
