#include <stdio.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include "hopman.h"
#include "watch.h"

void execute(command_t command, partinfo_t *p)
{
  static int debug = -1;
  char cmdbuf[1024];
  const char *cmdline;
  FILE *pfin = NULL;
  char *line;
  int len;
  pid_t pid;
  int wstatus;
  char short_command; /* if short command we wait for completion */

  if(debug == -1)
    {
      debug = config_bool(Debug);
      if(debug == -1) debug = 0;
    }
  
  cmdline = config_helper( command, p, cmdbuf, sizeof(cmdbuf) );
  if(!cmdline) goto nocmd;
  if(!*cmdline) goto nocmd;
  if(debug)
    fprintf(stderr, "External command to be issued: \"%s\"\n", cmdline);
		 
  switch(command)
    {
    case Open_in_File_Manager:
    case Open_in_Terminal:
      if( !p->nmounts ) execute(Mount, p); /* recursion ok for short command */
      pfin = epopen(cmdline, &pid, p->mnt);
      short_command = 0;
      break;
    case Mount:
      if(!p->nmounts)
	{
	  pfin = epopen(cmdline, &pid, "");
	  short_command = 1;
	}
      else ui_msg("already mounted.");
      break;
    case Unmount:
      if(p->nmounts)
	{
	  pfin = epopen(cmdline, &pid, "");
	  short_command = 1;
	}
      else ui_msg("not mounted.");
      break;
    case Eject:
      pfin = epopen(cmdline, &pid, "");
      short_command = 0;
      break;
    default:
      ui_msg("internal program error: invalid command.");
    }
  if(debug)
    fprintf(stderr, "External command issued: \"%s\"\n", cmdline);
  if(pfin && short_command)
    {
      line = fgets(cmdbuf, sizeof(cmdbuf), pfin);
      if(line)
	{
	  len = strlen(cmdbuf);
	  if(cmdbuf[len -1] == '\n') { cmdbuf[len -1] = '\0' ; len --; }
	  if(len)
	    {
	      if(debug) fprintf(stderr, "Message: \"cmdbuf\"\n", cmdbuf);
	      ui_msg(cmdbuf);
	    }
	}
      fclose(pfin);
      waitpid(pid, &wstatus, 0);
    }
  else if(pfin) fclose(pfin);
  mountpoints(); /* always update */
  return;
 nocmd:
  ui_msg("No command helper.");
  mountpoints(); /* always update */
  return;
}

/* Reap long lasting child processes */
void command_finished(pid_t pid, int wstatus)
{
  char message[80];

  message[0] = '\0';
  
  if(WIFEXITED(wstatus))
    {
      int status = WEXITSTATUS(wstatus);
      switch(status)
	{
	case EXIT_SUCCESS:
	  /* Don't bother ! */
	  break;
	case EXIT_FAILURE:
	  fprintf( stderr, _("%s helper command with pid %u failed.\n"),
		   progname, pid );
	  break;
	default:
	  fprintf(stderr,
		  _("%s helper command with pid %u terminated with status %d.\n"),
		  progname, pid, status );
	}
    }
  else if(WIFSIGNALED(wstatus))
    {
      int signal = WTERMSIG(wstatus);
      fprintf( stderr,
	       _("%s helper command with pid %u terminated by signal %d.\n"),
	       progname, pid, signal );
    }
  else if(WIFSTOPPED(wstatus))
    {
      int signal = WSTOPSIG(wstatus);
      fprintf( stderr, _("%s helper command with pid %u stopped by signal %d\n"),
	       progname, pid, signal );
    }
}
