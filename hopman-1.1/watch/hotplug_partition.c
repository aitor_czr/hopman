/* See LICENSE file for copyright and license details. */ 
/* Check if a device is a partition and its disk is removable */
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include "sysfs.h"
#include "watch.h"

int hotplug_partition(const char *name)
{
  char namebuf[NAME_MAX+1], *pathname;
  int devfd; /* file descriptors */
  struct stat buf;
  int rc, hoplug_ok = 0;
  
  if( !(pathname=devpath(name, namebuf)) )
    {
      fprintf(stderr, _("%s cannot find sys path for %s.\n"), progname, name);
      goto close0;
    }
 
  /* open directory pointed to by pathname */ 
  if( (devfd=open(pathname, O_RDONLY)) < 0 )
    {
      fprintf(stderr, _("%s cannot open %s: %s.\n"),
	      progname, name, strerror(errno));
      goto close0;
    }

  /* check the directory contains a file named "partition" */
  rc = fstatat( devfd, "partition", &buf, 0);
  if(rc) goto close1;
  
  rc = sysfs_get_parent_with_subsystem_devtype(pathname, "usb", "usb_interface");
  if(rc) goto close1;
  
  hoplug_ok = 1; 

  close1: close(devfd);
  close0: return hoplug_ok;
}
