/*
  This subprogram initializes the inotify filedescriptor and watches used
  to monitor the insertion and ejection of hot-plug disk partitions (USB keys
  or MMC devices). The main program is supposed to have performed a chdir to
  /dev before calling this subprogram.

  Two watches are created, one for file creation and deletion in /dev and one
  for file creation in /dev/disk/by-label, because the hotplugger (eg udev)
  creates in this directory symbolic links to the device files in /dev, named
  after the filesystem labels.
*/
#define _GNU_SOURCE
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/inotify.h>
#include <unistd.h>
#include "hopman.h"
#include "watch.h"
#include <limits.h>
#include <sys/stat.h>
#include <assert.h>
#include <stdbool.h>
#include "hopman.h"
#include "watch.h"

static int wd_dev, wd_label; /* the watch descriptors */
static int stat_errors = 0;
static int wd_dev_items = 0;
static bool iterate_all_devices = true;
static char Lname[NAME_MAX+1]={0};
static char Label[PM_LABEL_LEN+1]={0};

void do_partition_set_label_by_name();

// Usage: strncpy_t(str1, sizeof(str1), str2, strlen(str2));
// Copy string "in" with at most "insz" chars to buffer "out", which
// is "outsz" bytes long. The output is always 0-terminated. Unlike
// strncpy(), strncpy_t() does not zero fill remaining space in the
// output buffer:
static
char* strncpy_t(char* out, size_t outsz, const char* in, size_t insz)
{
   assert(outsz > 0);
   while(--outsz > 0 && insz > 0 && *in) { *out++ = *in++; insz--; }
   *out = 0;
   return out;
}

/*------------------ initialize file event notification -------------------*/
int inotinit(void)
{
  int inotfd;

  inotfd = inotify_init1(IN_CLOEXEC);
  if (inotfd < 0)
    {
      fprintf( stderr, _("%s cannot initialize inotify: %s\n"),
           progname, strerror(errno) );
      exit(EXIT_FAILURE);
    }
  wd_dev = inotify_add_watch(inotfd, ".", IN_CREATE | IN_DELETE);
  if(wd_dev == -1)
    {
      fprintf( stderr, _("%s cannot add inotify watch: %s\n"),
           progname, strerror(errno) );
      exit(EXIT_FAILURE);
    }
  wd_label = inotify_add_watch(inotfd, "disk/by-label", IN_CREATE);
  if(wd_label == -1)
    {
      fprintf(stderr, _("%s cannot watch directory disk/by-label: %s\n"),
          progname, strerror(errno));
      if(errno != ENOENT) exit(EXIT_FAILURE);
      /* errno = ENOENT means there is no hotplugger (udev/eudev/vdev) */
      /* able to create the symlinks to partitions' device files.      */
      /* We proceed without reporting labels.                          */
    }

  return inotfd;
}

/*------------------------ Read an inotify event ----------------------*/

/*======================== Process inotify events ===========================*/
void inotify_read(int inotfd)
{
  const size_t header_size = sizeof(int) + sizeof(uint32_t)*3;
  unsigned loops = 0, isloop = 0;
  struct inot_event
  {
    int wd;
    uint32_t mask;
    uint32_t cookie;
    uint32_t len;
    char name[NAME_MAX+1];
  };
  char buf[sizeof(struct inot_event)];
  struct stat devstat;
  char *b;
  int len;
  unsigned index;
  /* read inotify event(s) */

  len=read(inotfd, &buf, sizeof(struct inot_event));
#ifdef DEBUG_WATCH
  fprintf(stderr, "Read %u bytes from Inotiy\n", len);
#endif
  if(len >= header_size) /* we've read at least one event */
    {
      struct inot_event *inot_evt;
      /* We now have at least one inotify event in the buffer */
      for( b=buf, inot_evt=(struct inot_event*)b;
       len>= header_size;
       b += (header_size + inot_evt->len),
         len -= (header_size + inot_evt->len),
         inot_evt=(struct inot_event*)b )
    {
#ifdef DEBUG_WATCH
      isloop = !strncmp("loop", inot_evt->name, 4);
      if(isloop) loops ++;
#endif
      if(inot_evt->mask & IN_DELETE)
        {
#ifdef DEBUG_WATCH
          fprintf(stderr, "Inotify: delete %s\n", inot_evt->name);
#endif
          if(inot_evt->wd == wd_dev)
            partition_delete(inot_evt->name);
        }
      else if(inot_evt->mask & IN_CREATE)
        {
#ifdef DEBUG_WATCH
          fprintf(stderr, "Inotify: create %s\n", inot_evt->name);
#endif
          if(inot_evt->wd == wd_dev)
        {
          if( lstat(inot_evt->name, &devstat) )
            {
              fprintf(stderr,
                  _("%s warning: create unknown device %s.\n"),
                  progname, inot_evt->name);
              return;
            }
          if( (devstat.st_mode & S_IFMT) != S_IFBLK ) return;
          if( hotplug_partition(inot_evt->name) )
            {
              wd_dev_items ++;
              partition_new( devstat.st_ino, inot_evt->name );
            }
        }
          else if(inot_evt->wd == wd_label)
        { 
          char lname[NAME_MAX+1];
          unsigned index;
          strcpy(lname, "disk/by-label/");
          strncpy(lname+14, inot_evt->name, NAME_MAX-14);
          lname[NAME_MAX] = '\0';

          if(wd_dev_items == 0)
              iterate_all_devices = false;
          
          if(stat(lname, &devstat))
            {		 	
              stat_errors ++;
              perror(lname);
              if(!iterate_all_devices || stat_errors == wd_dev_items)
                {
                  do_partition_set_label_by_name();
                } 
              return;
            }
          
          stat_errors = 0;
          if(!iterate_all_devices) /* vdev */
            wd_dev_items = 0; 
          strncpy_t(Lname, NAME_MAX+1, lname, strlen(lname));
          strncpy_t(Label, PM_LABEL_LEN+1, inot_evt->name, strlen(inot_evt->name));
        }
#ifdef DEBUG_WATCH
          if(loops && !(loops%1000)) {fprintf(stderr,"%u loop devices\n",
                         loops);
#endif
        }
#ifdef DEBUG_WATCH
      if(len > header_size+inot_evt->len)
        fprintf(stderr, "inotify: %d more bytes in this inotify buffer\n",
            len - header_size - inot_evt->len);
#endif
    }
    }
  else fprintf( stderr, "%s error reading inotify event: %s\n",
        progname, strerror(errno) );
  return;
}

void do_partition_set_label_by_name()
{ 
  int rc;  
  char *temp = NULL;
  char *target = (char*) malloc(sizeof(char) * NAME_MAX+1);
  if(!target) 
    {
      fprintf(stderr, _("%s: Memory allocation failure.\n"), progname);
      exit(EXIT_FAILURE);
    }

  rc = readlink(Lname, target, NAME_MAX+1);    
  if(rc < 0)
    {
      fprintf(stderr, "[WARN]%s: readlink('%s') %s\n", 
                    progname, Lname, strerror(errno));
      free(target);    
      return;
    }
  temp = rindex(target, '/') + 1;
  partition_set_label_by_name (temp, Label);
  if(iterate_all_devices)
    { /* only for eudev */
      stat_errors = 0;
      wd_dev_items = 0;
    }
  free(target);  
}
