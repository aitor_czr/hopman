#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <stdlib.h>
#include <string.h>
#include "hopman.h"
#include "gui.h"

static guint32 u8tou32(const guint8* b)
{
  guint32 u;
  int i;
  for(u=*b, i=1; i<4; i++)
    {
      u <<= 8;
      u |= *(b+i);
    }
  return u;
}

/*============================== PIXBUF DATA ================================*/
#include "icon_32x32"

/*================= Destructor (dummy because data is static) ===============*/
void nop_destroy (guchar *pixels, gpointer data)
{
  /* NO OP */
}

void set_icon(GtkWindow *pW)
{
  GdkPixbuf *pPix;
  const char *magic = "GdkP";
  int i;
  guint32 total_len, pixdata_type, width, height, rowstride;
  const guint8 *pixeldata;
  
  if( strncmp(icon_32x32, magic, 4) )
    {
      fprintf(stderr, "%s error in function set_icon(): invalid Gdk pixbuf.\n",
	      progname);
      exit(EXIT_FAILURE);
    }

    total_len = u8tou32(icon_32x32+4);
    pixdata_type = u8tou32(icon_32x32+8);
    rowstride = u8tou32(icon_32x32+12);
    width  = u8tou32(icon_32x32+16);
    height = u8tou32(icon_32x32+20);
    pixeldata = icon_32x32 + 24;
    
    /* printf("total length=%u,\npixeldata_type=0x%x,\nrowstride=%u,\n"
	   "width=%u, height=%u.\n", total_len, pixdata_type, rowstride,
	   width, height); */

    pPix=gdk_pixbuf_new_from_data(pixeldata, GDK_COLORSPACE_RGB, TRUE, 8,
				  width, height, rowstride, nop_destroy, NULL);

  gtk_window_set_icon( pW, pPix );
}
