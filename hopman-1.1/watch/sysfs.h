/*
 * sysfs.h
 * 
 * Forked from vdev: a virtual device manager for *nix
 * Copyright (C) 2014  Jude Nelson
 * (https://github.com/jcnelson/vdev)
 * 
 * All modifications to the original source are:
 * Copyright (C) 2021 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 *
 * Original copyright and license text produced below.
 */

/*
   This file is dual-licensed: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3 or later as 
   published by the Free Software Foundation. For the terms of this 
   license, see LICENSE.GPLv3+ or <http://www.gnu.org/licenses/>.

   You are free to use this program under the terms of the GNU General
   Public License, but WITHOUT ANY WARRANTY; without even the implied 
   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.

   Alternatively, you are free to use this program under the terms of the 
   Internet Software Consortium License, but WITHOUT ANY WARRANTY; without
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   For the terms of this license, see LICENSE.ISC or 
   <http://www.isc.org/downloads/software-support-policy/isc-license/>.
*/


#ifndef __SYSFS_H__
#define __SYSFS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// sysfs methods
int sysfs_dev_if_packed_info(char const*, char*, size_t);
int sysfs_uevent_get_key(char const*, size_t, char const*, char**, size_t*);
int sysfs_get_parent_with_subsystem_devtype(char const*, char const*, char const*);
int sysfs_get_parent_device(char const*, char**, size_t*);

// file operations 
ssize_t io_read_uninterrupted(int, char*, size_t);
int io_read_file(char const*, char**, size_t*);

#endif // __SYSFS_H__
