#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "hopman.h"
int config_read(const char *);
int main(int argc, char **argv)
{
  char buf[1024];
  const char *name[N_CONFIG_PARAMS] = {PARAM_NAMES};
  partinfo_t bidon = {1234, 1, "foo5", "Tagada", "tmpfs", "/media/bar"};
  
  userinfo_init();
  
  if( config_read(argv[1]) )
    fprintf(stderr, "Error returned by config_read(): %s\n", strerror(errno) );
  else
    {
      paramid_t pi;
      for(pi=0; pi<N_CONFIG_PARAMS; pi++)
	{
	  const char *c = config_string(pi);
	  printf("%s: %p .. %p = ", name[pi], c, c+strlen(c));
	  if(pi<MountCheckPeriod) printf("%s\n", config_string(pi));
	  else if(pi == MountCheckPeriod) printf("%d\n", config_int(pi));
	  else if(pi == PidFile) printf("%s\n", config_string(pi));
	  else printf("%d\n", config_bool(pi));
	}
      fflush(stdout);
      printf("pid file = %s\n", config_pidfile(buf, sizeof(buf)));
      printf( "mount command=\"%s\"\n",
	      config_helper(Mount, &bidon, buf, sizeof(buf)) );
    }
  return 0;
}
