/* See LICENSE file for copyright and license details. */

#if !defined(ANNEX_H)
#define ANNEX_H
#include <sys/types.h>
#include "hopman.h"

/*
  The configuration data is shared between userinfo.c and config.c .
  The same buffer is allocated and re-allocated. The two functions are
  reallocation-safe: userinfo.c by keeping the offsets of its two strings,
  instead of pointers, and config.c by keeping track of the value of
  configbuf and updating its pointers in case of change. Therefore the
  calling order of the two functions only matters because userinfo is
  needed to determine the path of the configuration file. This is how it
  has been designed, but no wild reallocation of configbuf has been tried
  to check the safety of the two functions.
*/
# ifdef DECLARE_CONFIGBUF
char *configbuf;
size_t configlen;
# else
extern char *configbuf;
extern size_t configlen;
# endif
#endif
