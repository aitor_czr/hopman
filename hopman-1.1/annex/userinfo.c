/* See LICENSE file for copyright and license details. */ 
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <errno.h>
#include <string.h>
#include "hopman.h"
#include "annex.h"

/* offsets of username and home in configbuf: */
int name_offset, home_offset;
/* We save offsets rather than pointers because configbuf may be realloc()ed */

/*------------------------- obtain username and home ------------------------*/
int userinfo_init(void)
{
  char pwbuf[1024];
  struct passwd pw, *ppw;
  uid_t uid;
  size_t len, lenname, lenhome;
  char *newbuf;

  uid = getuid();
  errno = getpwuid_r(uid, &pw, pwbuf, 1024, &ppw);
  if( errno )
    {
      name_offset = home_offset = -1;
      return -1;
    }
  else if( !ppw)
    {
      name_offset = home_offset = -1;
      return 0;
    }

  lenname = strlen(ppw->pw_name);
  lenhome = strlen(ppw->pw_dir);

  /* We do not assume that configbuf is empty. At startup, configbuf is NULL
     and configlen is 0, but they may have been changed before userinfo_init 
     is invoked. */
  
  len = configlen + lenname + lenhome + 2;
  newbuf = realloc(configbuf, len);
  if(!newbuf)
    {
      name_offset = home_offset = -1;
      return -1;
    }

  configbuf = newbuf;

  /* store username and userhome at the end of configuration buffer */
  strcpy(configbuf+configlen, ppw->pw_name);
  strcpy(configbuf+configlen+lenname+1, ppw->pw_dir);
  name_offset = configlen;
  home_offset = configlen+lenname+1;
  configlen = len;
  return 0;
}

const char *username(void)
{
  if(name_offset == -1) return NULL;
  else return configbuf + name_offset;
}

const char *userhome(void)
{
  if(home_offset == -1) return NULL;
  else return configbuf + home_offset;
}

