/* See LICENSE file for copyright and license details. */

#if !defined(WATCH_H)
#define WATCH_H
#include <sys/types.h>
#include <time.h>
#include <stdio.h>
#include <signal.h>
#include "hopman.h"

typedef struct
{
  unsigned np;
  partinfo_t **p;
} partlist_t;
/* p is an array of pointers to partinfo_t variables; it is managed by
   realloc() to allow for an unlimited number of partitions, while the
   partinfo_t variables are managed by malloc() because they must stay at the
   same address for all their existence.
   p is used only in functions in this file. partinfo_t variables are used
   everywhere. A new partinfo_t variable is created for each newly detected
   partition and freed when the partition is removed. During all its existence,
   a partinfo_t must stay at the same address because the GUI holds a pointer
   to it.
*/

/* Partition management */
void partition_new( ino_t inode, const char *name );
void partition_delete( const char *name );
void partition_set_label( ino_t inode, const char *label );
void partition_set_label_by_name( const char *name, const char *label );
void partition_set_mountpoint( const char *name, const char *mountpoint,
			       const char *fstype, unsigned nmounts );
partlist_t partition_get_list(void);
unsigned partition_by_name( const char * );
void partition_print(void);

int   hotplug_partition (const char *devname); /* check device is a hp part. */
char *devpath(const char *devname, char *namebuf); /* device path in /sys */
void  stop_periodic(void);
void  command_finished(pid_t pid, int wstatus);
FILE *epopen(const char *command, pid_t *pid, const char *dirpath);

/* in file readwrite.c : read and write protected against EINTR */
int pm_read (int fd, void *buf, size_t count);
int pm_write(int fd, void *buf, size_t count);

#define NULL_PARTITION {0, 0, {0}, {0}, {0}}

/* Original signal mask of the process, must be given to children processses */
# ifdef DECLARE_OLDSET
sigset_t oldset;
# else
extern sigset_t oldset;
# endif

#endif
