/* Read the current directory (assuming it is the root of the device files
   tree (normally /dev) and invoke pm_add_part() for each entry of the
   directory. pm_add_part() will select the device files which make sense.
*/
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "hopman.h"
#include "watch.h"

static char slnk[NAME_MAX+1] = "disk/by-label/";

void init_list(void)
{
  DIR *dirp;
  struct dirent *E;

  /*------------------------------ Scan /dev --------------------------------*/
  dirp = opendir(".");
  if(!dirp)
    {
      fprintf(stderr, _("%s error opening current directory: %s\n"),
	      progname, strerror(errno));
      exit(EXIT_FAILURE);
    }

  /* Invoke partition_new() for all devices associated to hotplug partitions */
  while( (errno=0, E=readdir(dirp)) )
    {
      struct stat devstat;
      if( lstat(E->d_name, &devstat) )
	{
	  fprintf(stderr, _("%s warning: create unknown device %s\n"),
		  progname, E->d_name);
	  return;
	}
      if( (devstat.st_mode & S_IFMT) == S_IFBLK )
	{
	  if(hotplug_partition(E->d_name))
	    {
	      partition_new(E->d_ino, E->d_name);
	    }
	}
    }
  if(errno)
    {
      fprintf(stderr, _("%s error reading current directory: %s\n"),
	      progname, strerror(errno));
      exit(EXIT_FAILURE);
    }
  closedir(dirp);

  /*------------------------ Scan /dev/disk/by-label ------------------------*/
  dirp = opendir("disk/by-label");
  if(!dirp)
    {
      fprintf(stderr, _("%s cannot open subdirectory disk/by-label: %s\n"),
	      strerror(errno));
      return;
    }

  /* invoke partition_set_label() for every directory entry */
  while( (errno=0, E=readdir(dirp)) )
    {
      struct stat devstat;
      strcpy(slnk+14, E->d_name);
      if( stat(slnk, &devstat) ) continue;
      partition_set_label(devstat.st_ino, E->d_name);
    }
  if(errno)
    {
      fprintf(stderr, _("%s error reading current directory: %s\n"),
	      progname, strerror(errno));
      exit(EXIT_FAILURE);
    }
  closedir(dirp);

  /* check mountpoints */
  mountpoints();
  return;
}
