#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <gtk/gtk.h>
#include <linux/limits.h>
#include <signal.h>

#define MAIN_PROGRAM
#include "hopman.h"
#include "gui.h"

void printstatus(int status, void *arg);
gboolean callback_inotify( GIOChannel *source, GIOCondition condition,
			   gpointer pData );
gboolean callback_signal ( GIOChannel *source, GIOCondition condition,
			   gpointer pData );
gboolean callback_mounts ( GIOChannel *source, GIOCondition condition,
			   gpointer pData );
		       
void callback_hide( GtkWidget *widget, GdkEvent *event, gpointer *pData );
struct initial_position decode_pos(const char *);		       

/* Root of the device files tree: */
const char *watchdir="/dev";

gboolean decorate_window, toggle_hide, keep_position;

/* Type and global variable to handle initial position configuration */
struct initial_position
{
  enum position_style {None, Mouse, Percent} ip_sty;
  int ip_x, ip_y;
};

struct initial_position ipos;

/*================================== MAIN ===================================*/
int main(int argc, char **argv)
{
  static int inotfd, sfd, mntfd;
  static GIOChannel *ginotfd, *gsfd, *gmntfd;
  static guint rc;
  static time_t interval;
  
  /*-------------- make a global pointer to basename of argv[0] -------------*/
  {
    int i, l;
    l = strlen(argv[0]);
    for(i=l-1 ; argv[0][i] != '/' && i>0 ; i--) ;    /* find last '/' */
    if(argv[0][i] == '/') i++;
    progname = argv[0]+i;
  }

  /*---------------- Record printstatus() as on_exit function ---------------*/
  on_exit(printstatus, NULL);

  /*---------------------- Native Language Translation ----------------------*/
  {
    int j;
    const char *var[3] = {"LC_MESSAGES", "LC_ALL", "LANG"};
    const char *value, *mylocale;
    for(j=0; j<3; j++)
      {
	value=getenv(var[j]);
	if(value) break;
      }
    mylocale = setlocale(LC_MESSAGES, value);
    fprintf( stderr, _("%s: language set to %s.\n"), progname, mylocale );
  }

  /*-------------------------------------------------------------------------*/
  /*          Obtain username and home, read configuration file,             */
  /*            kill previous instance and write pid file.                   */
  do_all_config();

  /*----------------- Decode paramaters for future use ----------------------*/
  showflags = config_show();
  interval        = config_int(MountCheckPeriod);
  decorate_window = config_bool(Decorate);
  toggle_hide     = config_bool(ToggleHide);
  keep_position   = config_bool(KeepPosition);
  {
    const char *text;
    text = config_string(InitialPosition);
    if(text) ipos = decode_pos(text); else ipos.ip_sty = None;
  }
  
  /*------------------------- Initialize GTK --------------------------------*/
  gtk_init(&argc, &argv);
  
  /* create window */
  pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  set_icon (GTK_WINDOW(pWindow));
  /* icon shoule be of type GdkPixbuf * */
  if(ipos.ip_sty == Mouse)
    gtk_window_set_position(GTK_WINDOW(pWindow), GTK_WIN_POS_MOUSE );
  else
    gtk_window_set_position(GTK_WINDOW(pWindow), GTK_WIN_POS_NONE );
  /* make the window resizable - it also makes it movable */
  gtk_window_set_resizable(GTK_WINDOW(pWindow), TRUE);
  gtk_window_set_title(GTK_WINDOW(pWindow), "Hopman");

  /* set a minimum size so that the title is readable */
  gtk_window_set_default_size(GTK_WINDOW(pWindow), 200, 50);

  /* Optionally decorate the window (yes by default) */
  gtk_window_set_decorated (GTK_WINDOW(pWindow), decorate_window);

  /* Make the window sticky if requested */
  if( config_bool(Sticky) == 1 ) gtk_window_stick( GTK_WINDOW(pWindow) );

  /*--------------------- connect the destroy signal ------------------------*/
#if defined(DO_ICONIFY)
  /*g_signal_connect( G_OBJECT(pWindow), "destroy",
		    G_CALLBACK(callback_iconify), NULL ); */
  g_signal_connect_swapped(G_OBJECT(pWindow), "destroy",
			   G_CALLBACK(gtk_window_iconify),
			   GTK_WINDOW(pWindow));
#else
  g_signal_connect( G_OBJECT(pWindow), "destroy",
		    G_CALLBACK(gtk_main_quit), NULL);
#endif
  /*------------------------ Create the menubar -----------------------------*/
  pList = gtk_menu_bar_new();
  gtk_menu_bar_set_pack_direction(GTK_MENU_BAR(pList), GTK_PACK_DIRECTION_TTB);
  gtk_container_add(GTK_CONTAINER(pWindow), pList);
  
  /*-------------------- Make the watch directory current -------------------*/
  if( chdir(watchdir) )
    {
      perror(watchdir);
      return EXIT_FAILURE;
    }

  /*------ Initialize inotify & signalfd  and open /proc/self/mountinfo -----*/
  inotfd  = inotinit();
  ginotfd = g_io_channel_unix_new( inotfd );
  sfd     = signalinit();
  gsfd    = g_io_channel_unix_new( sfd );
  mntfd   = mountinit();
  gmntfd  = g_io_channel_unix_new( mntfd );
  
  /*----------------------- Declare input watches ---------------------------*/
  rc = g_io_add_watch(ginotfd, G_IO_IN,  callback_inotify, NULL);
  rc = g_io_add_watch(gsfd,    G_IO_IN,  callback_signal,  NULL);
  rc = g_io_add_watch(gmntfd,  G_IO_PRI, callback_mounts,  NULL);
  
  /*---------- report all hotplug partitions whic are alredy present --------*/
  init_list();

  /*----- If AutoHide is false and there is now partition, show window ------*/
  if( !(showflags & AUTOHIDE) ) ui_set_visible(1);

  /*---------------------------- Iddle loop ---------------------------------*/
  gtk_main();

  return EXIT_SUCCESS;
}

/*============================ Print exit status ============================*/
void printstatus(int status, void *arg)
{
  switch(status)
    {
    case EXIT_SUCCESS:
      fprintf(stderr, _("%s terminated successfully.\n"), progname);
      break;
    case EXIT_FAILURE:
      fprintf(stderr, _("%s failed.\n"), progname);
      break;
    default:
      fprintf(stderr, _("%s terminated with status=%d\n"), progname, status);
    }
}

/*========================= Channel input callbacks =========================*/
gboolean callback_inotify( GIOChannel *source, GIOCondition condition,
			   gpointer pData )
{
  int inotfd;

  inotfd = (int)g_io_channel_unix_get_fd(source);
  inotify_read(inotfd);
  return TRUE;
}

gboolean callback_signal ( GIOChannel *source, GIOCondition condition,
			   gpointer pData )
{
  int sfd;

  sfd = (int)g_io_channel_unix_get_fd(source);
  signal_read( sfd );
  return TRUE;
}

gboolean callback_mounts ( GIOChannel *source, GIOCondition condition,
			   gpointer pData )
{
  mountpoints();
  return TRUE;
}

/*========================== Hide callback ===============================*/
void callback_hide( GtkWidget *widget, GdkEvent *event, gpointer *pData )
{
  ui_set_visible(0);
}
		       
/*================== Set the visibility of the root window ==================*/
void ui_set_visible(int visibility)
{
  static gboolean show;
  static gint root_x, root_y;
  static gboolean known_pos;
  /* window_iconify has uknown effect and then window_deiconify crashes */
  /* If we never iconify then deiconify is harmless. Therefore we have  */
  /* commented iconification in main() and leaved deiconification here. */

  /* set initial position if defined */
  if(ipos.ip_sty == Percent && !known_pos)
    {
      gint win_width, win_height, screen_width, screen_height;
      gtk_window_get_size(GTK_WINDOW(pWindow), &win_width, &win_height);
#if GTK_MAJOR_VERSION == 2
      screen_width = gdk_screen_width();
      screen_height = gdk_screen_height();
#elif GTK_MAJOR_VERSION == 3
      GdkRectangle workarea = {0};
      gdk_monitor_get_workarea
        (
          gdk_display_get_primary_monitor(gdk_display_get_default()),
          &workarea
        );
      screen_width = workarea.width;
      screen_height = workarea.height;  
#endif
      root_x = (screen_width -win_width ) * ipos.ip_x / 100;
      root_y = (screen_height-win_height) * ipos.ip_y / 100;
      known_pos = TRUE;
    }

  if(visibility == -1 && !toggle_hide) visibility = 1;

  switch(visibility)
    {
    case 0:
      show = FALSE;
      break;
    case 1:
      show = TRUE;
      break;
    case -1:
      show = (!show);
      break;
    default:
      show = TRUE;
    }
  if(show)
    {
      gtk_widget_show_all( pWindow );
      gtk_window_present( GTK_WINDOW(pWindow) );
      if(known_pos)
	{
	  gtk_window_move( GTK_WINDOW(pWindow), root_x, root_y);
	}
    }
  else
    {
      gtk_window_deiconify( GTK_WINDOW(pWindow) );
      if(keep_position)
	{
	  gtk_window_get_position( GTK_WINDOW(pWindow), &root_x, &root_y );
	  known_pos = TRUE;
	}    
#if GTK_MAJOR_VERSION == 2
      gtk_widget_hide_all( pWindow );
#elif GTK_MAJOR_VERSION == 3
      gtk_widget_hide( pWindow );  
#endif
    }
}

/*===== Interpret a character string defining a position on the screen ===== */
/* The position can be a pair of integers separated by white space or the    */
/* the single word "Mouse" (case independant).                               */
/* The integers' range is [0,100] .                                          */
/* All texts or values not matching these constraints will result in the     */
/* position being undefined.                                                 */

struct initial_position decode_pos(const char *text)
{
  struct initial_position ip;
  int n;
  const char *style[3]={"None", "Mouse", "Percent"};
  ip.ip_sty = None;
  n = sscanf(text, " %d %d ", &ip.ip_x, &ip.ip_y);
  if(n==2)
    {
      if(ip.ip_x>=0 && ip.ip_x<=100 && ip.ip_y>=0 && ip.ip_y <=100)
	ip.ip_sty = Percent;
    }
  else if( !strcasecmp("mouse", text) ) ip.ip_sty = Mouse;

  return ip;
}
