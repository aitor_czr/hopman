/* See LICENSE file for copyright and license details. */
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "hopman.h"
#include "watch.h"

/*=============================== WARNING ===================================*/
/* Beware: realloc() is used to increase/decrease the size of the memory     */
/* area used to store partition data, but realloc() may move this memory     */
/* area. Therefore pointers to partition's info are short-lived and cannot   */
/* be retained by the GUI, eg, for use by the callbacks.                     */
/*======================== THIS IS STILL TO BE DONE==========================*/
static partlist_t P = {0, NULL};
static int debug = -1;

/*------------------------------- Creator -----------------------------------*/
void partition_new( ino_t inode, const char *name )
{
  if(debug==-1)
    {
      debug = config_bool(Debug);
      if(debug == -1) debug = 0;
    }
  /* reallocate memory for the new array of partinfo_t pointers */
  P.p = realloc( P.p, sizeof(partinfo_t *) * (P.np+1) );

  if( P.np && !P.p )
    {
      fprintf( stderr, _("%s Cannot store partition data: %s\n"),
	       progname, strerror(errno) );
      exit( EXIT_FAILURE );
    }
  
  /* Allocate memory for this new partition */
  P.p[P.np] = malloc( sizeof(partinfo_t) );
  if(!P.p[P.np])
    {
      fprintf( stderr, _("%s Cannot store partition data: %s\n"),
	       progname, strerror(errno) );
      exit( EXIT_FAILURE );
    } 
  /* set the inode and name of the new partition */
  P.p[P.np]->inode = inode;
  strncpy(P.p[P.np]->name, name, PM_NAME_LEN);
  P.p[P.np]->label[0] = P.p[P.np]->label[PM_LABEL_LEN] = '\0';
  P.p[P.np]->fstype[0] = P.p[P.np]->fstype[PM_FSTYPE_LEN] = '\0';
  P.p[P.np]->mnt[0] = P.p[P.np]->label[PM_MNT_LEN] = '\0';
  P.p[P.np]->nmounts = 0;
  
  /* Tell ui we have a new partition */
  ui_create(P.p[P.np]);

  P.np ++;
  if(debug) partition_print();

  return;
}

/*------------------------------- Destructor --------------------------------*/
void partition_delete( const char *name )
{
  unsigned index;

  /* Find partition */
  for(index=0; index<P.np; index ++) if(!strcmp(P.p[index]->name, name)) break;
  if(index == P.np) return;

  /* Tell ui to remove partition from display */
  ui_delete(P.p[index]);

  /* free partition's data */
  free(P.p[index]);

  -- P.np;
  
  /* reshuffle the list */
  for( ; index < P.np; index ++) P.p[index] = P.p[index+1];
  
  /* reallocate memory for the new number of partitions */
  P.p = realloc( P.p, sizeof(partinfo_t *) * P.np );

  if( P.np && !P.p )
    {
      fprintf(stderr,_("%s Error freeing memory, returned by realloc(): %s\n"),
	       progname, strerror(errno) );
      exit( EXIT_FAILURE );
    }

  if(debug) partition_print();
  return;
}

/*------------------------------- Getters -----------------------------------*/
partlist_t partition_get_list(void)
{
  /* for use in mountpoints.c - dangerous, use with care ! */
  return P;
}

unsigned partition_by_name( const char *name )
{
  unsigned index;
  for(index=0; index<P.np; index++)
    {
      if( !strcmp(P.p[index]->name, name) ) break;
    }
  return index; /* If partition isn't found, return number of partitions */
}

unsigned partition_by_inode( ino_t inode )
{
  unsigned index;
  for(index=0; index<P.np; index++)
    {
      if( P.p[index]->inode == inode ) break;
    }
  return index; /* If partition isn't found, return number of partitions */
}

partinfo_t *get_partition( ino_t inode)
{
  int index;
  index = partition_by_inode(inode);
  return P.p[index];
}

/*--------------------------------- Setters ---------------------------------*/
void partition_set_label( ino_t inode, const char *label )
{
  unsigned index;
  int changed = 0;
  index = partition_by_inode(inode);
  if(index < P.np)
    {
      if( strcmp(P.p[index]->label, label) )
	{
	  strncpy(P.p[index]->label, label, PM_LABEL_LEN);
	  ui_update(P.p[index]);
	  changed ++;
	}
    }
  
  if(debug && changed) partition_print();
}

void partition_set_label_by_name( const char *name, const char *label )
{
  unsigned index;
  int changed = 0;
  index = partition_by_name(name);
  if(index < P.np)
    {
      if( strcmp(P.p[index]->label, label) )
	{
	  strncpy(P.p[index]->label, label, PM_LABEL_LEN);
	  ui_update(P.p[index]);
	  changed ++;
	}
    }
  
  if(debug && changed) partition_print();
}

/*-------------------------------- Debugger ---------------------------------*/
void partition_print(void)
{
  int i;
  if(P.np)
    fprintf(stderr,"   Inode Nm Device-Name    Label          Fstype        "
	    " Mount-Point\n");
  for( i=0; i<P.np; i++ )
    {
      fprintf(stderr, "%8d %2d %-14s %-14s %-14s %-.20s\n",
	      P.p[i]->inode, P.p[i]->nmounts, P.p[i]->name, P.p[i]->label,
	      P.p[i]->fstype, P.p[i]->mnt);
    }
}

